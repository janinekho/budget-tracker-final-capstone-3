import Head from 'next/head'
import React, { useState, useEffect } from 'react';

export default function Home() {
  const [number1, setNumber1] = useState("");
  const [number2, setNumber2] = useState("");
  const [number3, setNumber3] = useState("");
  /*const [checked1, setChecked1] = useState(false);
  const [checked2, setChecked2] = useState(false);
  const [checked3, setChecked3] = useState(false);*/
  const [clear,setClear]=useState(false);
  const [result, setResult] = useState("0");

  function calculate(operator = "") {
    let list = []

     list.push(number1)
     list.push(number2)
     list.push(number3)

    console.log(list)
    console.log(number1);
    
    /*if (list.length === 0) {
      setResult("Please select fields");
      return
    }

    if (list.length === 1) {
      setResult("At least need 2 fields selected");
      return
    }*/

    let total = list[0];

    for (let idx = 1; idx < list.length; idx++) {
       let number = list[idx];
       if (number === ""){
         continue;
       } else{
      if (operator==="+") total += number;
      if (operator==="-") total -= number;
      if (operator==="*") total *= number;
      if (operator==="/") total /= number;
      console.log(operator);
      }
    }
    
    setResult(total)
    console.log(total);
    console.log(result);
  }

  useEffect(()=>{
    if(clear)
    document.querySelector('.jsx-946912577').value="";
  })

  function clearAll(){
    setClear(true);
    setResult(0);
  }

  return (
    <div className="calculator">
      <Head>
        <title>Calculator App</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />
      </Head>

      <main>
        <div className="App">

          <div className="container-form">
            <br /><br />
            <h2>Widget Calculator</h2> 
            <br /><br />
            
            <div className="inline-form">
              <div>
                <input
                  type="number"
                  value={number1}
                  onChange={e => setNumber1(+e.target.value)}
                  
                  className="form-control"
                />
              </div>
              
            </div>

            <div className="inline-form">
              <div>
                <input
                  type="number"
                  value={number2}
                  onChange={e => setNumber2(+e.target.value)}
                  
                  className="form-control"
                />
              </div>
              
            </div>

            <div className="inline-form">
              <div>
                <input
                  type="number"
                  value={number3}
                  onChange={e => setNumber3(+e.target.value)}
                  
                  className="form-control"
                />
              </div>
             
            </div>
            <br />
            <div className="inline-form">
              <button type="button" className="btn btn-primary" onClick={() => {calculate('+')}}> + </button>
              <button type="button" className="btn btn-primary" onClick={() => {calculate('-')}}> - </button>
              <button type="button" className="btn btn-primary" onClick={() => {calculate('*')}}> * </button>
              <button type="button" className="btn btn-primary" onClick={() => {calculate('/')}}> / </button>
               <button type="button" className="btn btn-primary" onClick= {clearAll}>CE</button>
            </div>
            <br />
          </div>
        
          <br />
          TOTAL:
          <h2>{result}</h2>
        </div>
    
      </main>

      <style jsx>{`
        .App {
          text-align: center;
        }
        
        .container-form {
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        
        .inline-form {
          display: flex;
          flex-direction: row;
        }
        
        .inline-form > div > label  {
          margin-left : 15px;
        }
        
        div.form-control-lg  {
          padding : .5rem 0;
          padding-left: 2.25rem;
        }
        
        .inline-form > button  {
          width: 50px;
          margin : 10px;
        }
        
        .inline-form > button:first-child  {
          margin-left : 0px;
        }
        
        label {
          cursor : pointer;
        }
        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }
        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  )
}

