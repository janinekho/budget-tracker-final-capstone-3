import React from 'react'

export default function footer(){

return(
<React.Fragment>

<div className="fixed-bottom">
       <div className="footer">
       <h6 className="rights">&copy; Janine Kristen Kho 2020 | All Rights Reserved</h6>
       <h6 className="verse"> Luke 8:15 But the seed in the good soil, these are the ones who have heard the word in an honest and good heart, and hold it fast, and bear fruit with perseverance. </h6>
      </div>
 </div>
 </React.Fragment>
)
}