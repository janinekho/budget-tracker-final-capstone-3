import React,{ useContext, useState , useEffect} from 'react';
import { Button, Card, Form,Table,Col } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import {ListGroup} from 'react-bootstrap';

export default function Categories(){
const [type, setType] = useState('');
const [description, setDescription]= useState('');
const [category, setCategory] = useState([]);

  function submit(e){   
      fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/add-category`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json', 
               
        },
        body: JSON.stringify({
          type: type,
          description: description
        })
        
        })
      .then(res => res.json())
      .then(data => {       
         console.log(data);
         
    	})
     }
     console.log(type)
         console.log(description)



    useEffect(() =>{
      fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/category`, {
        method: 'GET',
        headers: {
             
            Authorization : `Bearer ${ AppHelper.getAccessToken()}`   
        },
        
        })     
      .then(res => res.json())
      .then(data => {       
         
         setCategory(data);
         
      })
     

    },[])
    console.log(category)

    const [isOpen, setIsOpen] = useState(false)

	return(
	<React.Fragment>
   
		<h3> New Category </h3>
    <div className="newCategory">
	
	<Form onSubmit={e => submit(e)}>
	<Form.Group>
         <Form.Label> Category Type: </Form.Label>
           	<Form.Control as="select" value={type} onChange= {e => setType(e.target.value)}>>
           		<option value="" > - </option>
                <option value="Income" >Income </option>
      			<option value="Expense">Expense</option>			
            </Form.Control>
     </Form.Group>
     <Form.Group>
         <Form.Label> Category Name: </Form.Label>
           	<Form.Control as="textarea"
           	value={description}
            onChange={e => setDescription(e.target.value)}>			
            </Form.Control>
     </Form.Group>
     <button> Submit </button>
     </Form>
    
     </div>

      <React.Fragment>
     <Table>
        <thead>
        

        <tr>
         
         <th> Transaction Type </th>
         <th> Transaction Description </th>
        
        </tr>
       
        </thead>

        {category.map((category) =>{ 
          return(
        <tbody>
        
        <td class="withedit">
        <button className="editbutton" value={category._id}> Edit </button><div>{category.type}</div>
        
        </td>

        <td>
        {category.description}
        </td>
        </tbody>
        )
        })}
      </Table>

      </React.Fragment> 
     

    </React.Fragment>
    )

}

