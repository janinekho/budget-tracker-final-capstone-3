import React,{useContext, useState, useEffect} from 'react';
import { Form, Button, Row, Col,Card } from 'react-bootstrap';
import Head from 'next/head';
import Router from 'next/router';
import UserContext from '../../UserContext';
import View from '../../components/View';
import AppHelper from '../../app-helper';



export default function edit() {
     const{ user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [firstName, setfirstName]= useState('');
  const[lastName, setlastName]= useState('');
  const [newFName, setFName]= useState('');
  const[newLName, setLName]= useState('');
  const[budget,setBudget]= useState('');



  useEffect(()=>{
    
    const options = {
     headers: {Authorization : `Bearer ${ AppHelper.getAccessToken()}`}
    }
    fetch(`${process.env.API_URL}/users/details`, options)
      .then (AppHelper.toJSON)
      .then(userData => {
        if (typeof userData._id !== 'undefined'){
          setUser({
            id: userData._id,
            isAdmin: userData.isAdmin,
            firstName: userData.firstName,
            lastName: userData.lastName,
            email: userData.email

          })

          setfirstName(userData.firstName)
          setlastName(userData.lastName)
        
         
      }
    })
},[])
  
  function editProfile(e){
    e.preventDefault();
  fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/edit-user`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body:{
          'firstName': firstName,
          'lastName': lastName
        }
        
        })
      .then(res => res.json())
      .then(data => {
        console.log(data)


})

  }

  console.log(firstName)
    return(

    <div>
    
   <View title ={'Edit'}>
    <Row className="justify-content-center">
      <Col xs md="6">
        <h3>Edit User</h3>  
        
      </Col>
    </Row>

 
     </View>  
    


      
      <Card>
        
          <Card.Body>
             <Form onSubmit={e => authenticate(e)}>
        <Form.Group>
            <Form.Label>First Name:</Form.Label>
            <Form.Control 
                type="text" 
                placeholder={firstName}
              onChange={(e) => setfirstName(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group>
            <Form.Label>Last Name:</Form.Label>
            <Form.Control 
                type="text" 
                placeholder={lastName}
                onChange={(e) => setlastName(e.target.value)}
            />
        </Form.Group>

        
           <button onClick={editProfile}> Submit </button>
          </Form>  
          </Card.Body>
        </Card>  
    </div>
         
  )
}